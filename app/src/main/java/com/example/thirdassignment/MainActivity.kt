package com.example.thirdassignment

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.example.thirdassignment.databinding.ActivityMainBinding
import java.util.regex.Pattern
import java.util.regex.Pattern.compile
import android.widget.EditText




class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        binding.btnSave.setOnClickListener {
            if(validateFields())
                Toast.makeText(this,"You data has been saved successfully",Toast.LENGTH_SHORT).show()
        }


        binding.btnClear.setOnLongClickListener {

            run {
                var i = 0
                val count: Int = binding.root.getChildCount()
                while (i < count) {
                    val view: View = binding.root.getChildAt(i)
                    if (view is EditText) {
                        (view as EditText).setText("")
                    }
                    ++i
                }
            }
            true
        }
    }


    private fun validateFields():Boolean
    {
        if(binding.etEmail.text.toString().isEmpty() || binding.etUsername.text.toString().isEmpty() ||
                binding.etFirstName.text.toString().isEmpty() || binding.etLastName.text.toString().isEmpty() ||
                binding.etAge.text.toString().isEmpty())
        {
            Toast.makeText(this,"Please,Fill all Fields",Toast.LENGTH_SHORT).show()
            return false


        } else if(binding.etUsername.text.length < 10)
        {
            Toast.makeText(this,"Username should not be less than ten symbols",Toast.LENGTH_SHORT).show()
            return false

        } else if(!emailValidator())
        {
            Toast.makeText(this,"Enter a valid Email",Toast.LENGTH_SHORT).show()
            return false

        } else if(binding.etAge.text.toString().toInt() < 0)
        {
            Toast.makeText(this,"No negative numbers are allowed",Toast.LENGTH_SHORT).show()
            return false
        }


        return true
    }


    //to validate email
    private fun emailValidator():Boolean
    {
        val emailRegex = compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
        )

        return emailRegex.matcher(binding.etEmail.text.toString()).matches()
    }
}


